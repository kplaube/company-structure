# Company Structure

This is Veo's Full Stack Code Challenge, implemented in Python+Flask and Javascript+React.

## Getting started

**Disclaimer:** The `make` tasks will only work with `poetry`, therefore, it's recommended to [have it installed](https://python-poetry.org/docs/#installation). Still, a `requirements.txt` file is being generated if you want to [run it by setting your own Python virtual environment](https://docs.python.org/3/library/venv.html).

To have all the dependencies installed, use the `install` task:

```text
make install
```

It will deal with both Python and Javascript/Node dependencies.

And to run Flask's development server, use `run`:

```text
make -j2 run 
```

It will run backend and frontend at once in parallel. If you want to run than in isolation:

```text
make run-backend
make run-frontend
```

To run the backend tests, a `test` task is available:

```text
make test
```