import axios from 'axios';
import { useState } from 'react';
import { EMPLOYEES_URL } from '../constants';

export default ({ data, refreshData }) => {
  const [form, setForm] = useState({
    name: '',
    supervisor_id: ''
  });

  const handleSubmit = async (e) => {
    e.preventDefault();

    // Rudimentary validation
    if (form.name === '') {
      alert('Name is required');
      return;
    }

    const payload = { name: form.name };
    if (form.supervisor_id !== '') {
      payload.parent_id = form.supervisor_id;
    }

    await axios.post(EMPLOYEES_URL, payload);

    setForm({
      name: '',
      supervisor_id: ''
    });

    refreshData();
  };

  return (
    <form className="mx-auto w-3/6" onSubmit={handleSubmit}>
      <h2 className="mb-4 text-center text-2xl">Add new employee</h2>
      <div>
        <label className="block mb-2">Name:</label>
        <input className="border-2 border-gray-200 mb-4 p-2 rounded w-full" type="text" value={form.name} onChange={(e) => setForm({...form, name: e.target.value })} />
      </div>
      <div>
        <label className="block mb-2">Supervisor:</label>
        <select className="border-2 border-gray-200 mb-4 p-2 rounded w-full" onChange={(e) => setForm({...form, supervisor_id: e.target.value })}>
          <option value="">-- Set as CEO --</option>
          {data && Object.keys(data).filter((key) => /^\d$/.test(key)).map((key) => (
            <option key={key} value={key}>{data[key].name}</option>
          ))}
        </select>
        <button type="submit" className="bg-blue-500 p-4 rounded text-white w-full">Add new</button>
      </div>
    </form>
  );
}