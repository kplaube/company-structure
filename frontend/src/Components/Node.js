import { useState } from "react";
import Badge from "./Badge";
import EditForm from "./EditForm";

const recursiveNode = (employee, tree, first, editMode, setEditMode, refreshData) => {
  return (
    <li key={employee.id} className="pl-4">
      <div className="hover:bg-gray-50 border-2 border-gray-100 block flex p-4 rounded">
        <span className="flex-1">{employee.name} {first && <Badge text="CEO" />}</span>
        <button
          className="hover:cursor-pointer hover:underline text-blue-500"
          onClick={(e) => { setEditMode(editMode => employee.id) }}
        >Edit</button>
      </div>
      {editMode && editMode == employee.id && (
        <EditForm employee={employee} tree={tree} setEditMode={setEditMode} refreshData={refreshData} />
      )}
      {employee.subordinates.length > 0 && (
        <ul className="border-l-2 border-dashed py-4">
          {
            employee.subordinates.map(
              (node_id) => tree[node_id] && recursiveNode(tree[node_id], tree, false, editMode, setEditMode, refreshData)
            )
          }
        </ul>
      )}
    </li>
  )
}

export default ({ employee, tree, refreshData }) => {
  const [editMode, setEditMode] = useState(null);

  return recursiveNode(employee, tree, true, editMode, setEditMode, refreshData);
}