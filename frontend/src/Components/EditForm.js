import axios from 'axios';
import { useState } from "react";
import { EMPLOYEES_URL } from '../constants';

export default ({ employee, tree, setEditMode, refreshData }) => {
  const [form, setForm] = useState({
    name: employee.name,
    parent_id: employee.parent_id
  })

  const closeForm = (e) => {
    e.preventDefault();
    setEditMode(editMode => ({
      ...editMode,
      [employee.id]: false
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    console.log(form)

    // Rudimentary validation
    if (form.name === '') {
      alert('Name is required');
      return;
    }

    const payload = { name: form.name };
    if (form.parent_id !== '') {
      payload.parent_id = form.parent_id;
    }

    const employee_url = `${EMPLOYEES_URL}${employee.id}/`;
    await axios.put(employee_url, payload);

    setForm({
      name: "",
      parent_id: "",
    });

    setEditMode({})
    refreshData();
  };

  return (
    <form className="border-2 p-2 flex" onSubmit={handleSubmit}>
      <div className="flex-1">
      <input
        type="text"
        className="border-2 border-gray-200 p-2 rounded"
        defaultValue={form.name}
        onChange={(e) => setForm((form) => ({...form,  name: e.target.value }))}
      />

        <select
          className="border-2 border-gray-200 p-2 rounded"
          defaultValue={form.parent_id}
          onChange={(e) => setForm((form) => ({...form,  parent_id: e.target.value }))}
        >
          <option value="">-- Set as CEO --</option>
          {tree && Object.keys(tree).filter((key) => /^\d$/.test(key) && key != employee.id).map((key) => (
              <option key={key} value={key}>{tree[key].name}</option>
          ))}
        </select>
      </div>
      
      <button
        onClick={closeForm}
        className="bg-gray-400 p-2 rounded text-white w-1/5"
      >Cancel</button>
      <button className="bg-blue-500 ml-2 p-2 rounded text-white w-1/5">Save</button>
    </form>
  );
};