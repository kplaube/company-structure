export default ({ text }) => (
  <div className="bg-red-500 inline-block p-2 rounded text-xs text-white">{text}</div>
);