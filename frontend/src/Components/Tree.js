import axios from 'axios';
import { useEffect, useState } from 'react';
import { EMPLOYEES_URL } from '../constants';
import AddForm from './AddForm';
import Node from './Node';

const fetchData = async (setData, url, depth = 0, parent_id = null) => {
  const result = await axios.get(url);
  const node = result.data.data;

  node.parent_id = parent_id
  node.weight = depth;

  if (depth == 0) {
    setData({
      root: node.id,
      [node.id]: node
    });
  } else {
    setData(prevData => ({
      ...prevData,
      [node.id]: node
    }));
  }

  node.subordinates && node.subordinates.forEach(async (node_id) => {
    await fetchData(setData, `${EMPLOYEES_URL}${node_id}/`, depth + 1, node.id);
  });
};

export default () => {
  const [data, setData] = useState({});
  const refreshData = () => {
    fetchData(setData, EMPLOYEES_URL);
  };

  useEffect(() => {
    refreshData();
  }, [EMPLOYEES_URL]);

  return (
    <div>
      {data && (
        <ul>
          {data[data.root] && <Node tree={data} employee={data[data.root]} refreshData={refreshData} />}
        </ul>
      )}
      <hr className="my-8" />

      <AddForm data={data} refreshData={refreshData}/>
    </div>
  )
}