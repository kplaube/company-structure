import Tree from './Components/Tree';

export default () => (
    <div className="p-8">
        <h1 className="mb-8 text-center text-4xl">Company structure</h1>
        <Tree />
    </div>
);