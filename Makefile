POETRY_CMD = poetry run


install:
	poetry install
	cd frontend; npm install

lint:
	$(POETRY_CMD) flake8 .
	$(POETRY_CMD) mypy .

run: run-frontend run-backend

run-frontend:
	cd frontend; npm run start

run-backend:
	$(POETRY_CMD) flask run --reload

test:
	$(POETRY_CMD) pytest