class TestTreeAdd:
    def test_should_generate_a_node(self, tree):
        node = tree.add("new node")

        assert node.id > 0
        assert node.name == "new node"

    def test_should_add_a_node_to_the_root(self, tree):
        old_root = tree.root

        node = tree.add("new root")

        assert tree.root == node
        assert old_root in tree.root.children

    def test_should_add_a_node_as_a_leaf(self, tree):
        node = tree.add("new leaf", 6)

        assert node.parent.id == 6
        assert node.parent.children == [node]
        assert not node.children


class TestTreeFind:
    def test_should_find_root_element(self, tree):
        node = tree.find(1)

        assert node.name == "root"

    def test_should_find_element_in_the_middle_of_the_tree(self, tree):
        node = tree.find(3)

        assert node.name == "c"

    def test_should_find_a_leaf(self, tree):
        node = tree.find(6)

        assert node.name == "b"

    def test_should_return_none_when_node_is_not_found(self, tree):
        node = tree.find(10)

        assert node is None


class TestTreeSetParent:
    def test_should_update_node_parent(self, tree):
        parent_node = tree.find(1)
        new_parent, node = parent_node.children

        node = tree.set_parent(node, new_parent.id)

        assert node.parent == new_parent

    def test_should_update_children_references(self, tree):
        parent_node = tree.find(1)
        new_parent, node = parent_node.children

        node = tree.set_parent(node, new_parent.id)

        assert node not in parent_node.children
        assert node in new_parent.children

    def test_should_set_node_as_root_when_id_is_missing(self, tree):
        old_root_node = tree.find(1)
        node = tree.find(6)

        node = tree.set_parent(node, None)

        assert tree.root == node
        assert node.parent is None
        assert old_root_node.parent == node
        assert node not in old_root_node.children
        assert old_root_node in node.children
