import pytest

from company_structure import create_app
from company_structure.services import static_tree


@pytest.fixture(autouse=True)
def tree():
    yield static_tree

    # Reset tree after each test
    static_tree.reset()


@pytest.fixture
def app():
    app = create_app()
    return app


@pytest.fixture
def client(app):
    return app.test_client()
