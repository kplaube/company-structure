class TestAddEmployee:
    def test_should_succesfully_post_to_the_view(self, client):
        response = client.post("/employees/", json={"name": "new root"})

        assert response.status_code == 201

    def test_should_return_the_created_employee(self, client):
        response = client.post("/employees/", json={"name": "new root"})
        data = response.get_json()

        assert type(data["data"]["id"]) == int
        assert data["data"]["name"] == "new root"
        assert data["data"]["subordinates"] == [1]

    def test_should_return_bad_request_when_name_is_missing(self, client):
        response = client.post("/employees/", json={})
        data = response.get_json()

        assert response.status_code == 400
        assert "validation_error" in data

    def test_should_return_bad_request_when_parent_id_is_invalid(self, client):
        response = client.post(
            "/employees/", json={"name": "new root", "parent_id": 100}
        )
        data = response.get_json()

        assert response.status_code == 400
        assert "validation_error" in data


class TestEmployee:
    def test_should_access_the_view(self, client):
        response = client.get("/employees/1/")

        assert response.status_code == 200

    def test_should_return_employee_details_and_its_subordinates(self, client):
        response = client.get("/employees/1/")
        data = response.get_json()

        assert data["data"] == {
            "id": 1,
            "name": "root",
            "subordinates": [2, 6],
        }

    def test_should_return_root_when_node_id_is_missing(self, client):
        response = client.get("/employees/")
        data = response.get_json()

        assert data["data"]["name"] == "root"

    def test_should_return_not_found_when_node_does_not_exist(self, client):
        response = client.get("/employees/10/")

        assert response.status_code == 404


class TestUpdateEmployee:
    def test_should_successfully_put_the_view(self, client):
        response = client.put("/employees/1/", json={"name": "new root"})

        assert response.status_code == 200

    def test_should_update_the_nome_details(self, client):
        response = client.put("/employees/1/", json={"name": "new root"})
        data = response.get_json()

        assert data["data"]["name"] == "new root"

    def test_should_set_node_as_new_root_if_parent_id_is_not_informed(self, client):
        response = client.put("/employees/2/", json={"name": "new root"})
        data = response.get_json()

        assert data["data"]["name"] == "new root"
        assert set(data["data"]["subordinates"]) == {1, 3}

    def test_should_set_parent_when_id_is_informed(self, client, tree):
        client.put("/employees/2/", json={"name": "new a", "parent_id": 6})

        root_node = tree.find(1)
        updated_node = tree.find(2)

        assert updated_node not in root_node.children
        assert updated_node.parent.id == 6

    def test_should_return_bad_request_when_name_is_missing(self, client):
        response = client.put("/employees/2/", json={})
        data = response.get_json()

        assert response.status_code == 400
        assert "validation_error" in data

    def test_should_return_bad_request_when_parent_id_is_invalid(self, client):
        response = client.put("/employees/2/", json={"name": "new a", "parent_id": 100})
        data = response.get_json()

        assert response.status_code == 400
        assert "validation_error" in data

    def test_should_return_not_found_when_node_does_not_exist(self, client):
        response = client.put(
            "/employees/100/", json={"name": "should-not-exist", "parent_id": 1}
        )

        assert response.status_code == 404
