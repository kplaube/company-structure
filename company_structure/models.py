from typing import Optional

from .utils import NodeCounter


class Node:
    def __init__(self, name: str):
        self.name = name
        self.parent: Optional["Node"] = None
        self._children: list["Node"] = []
        self._id = NodeCounter.get_id()

    def __repr__(self):
        return "Node(id={}, name={}, parent={})".format(
            self.id,
            self.name,
            self.parent,
        )

    @property
    def children(self) -> list["Node"]:
        return self._children

    @property
    def id(self) -> int:
        return self._id

    def add_child(self, node: "Node") -> "Node":
        node.parent = self
        self._children.append(node)

        return node

    def remove_child(self, node: "Node") -> "Node":
        self._children.remove(node)

        return node

    def update(self, name: str):
        self.name = name

    def to_dict(self, depth=True):
        result = {
            "id": self.id,
            "name": self.name,
        }

        if depth:
            result["subordinates"] = [child.id for child in self.children]

        return result
