class NodeCounter:
    __autoincrement_counter = 0

    @classmethod
    def get_id(cls):
        cls.__autoincrement_counter += 1
        return cls.__autoincrement_counter

    @classmethod
    def reset(cls):
        cls.__autoincrement_counter = 0
