from flask import Blueprint
from flask_cors import CORS
from flask_pydantic import validate

from .models import Node
from .services import static_tree
from .validators import NodeCreateModel

blueprint = Blueprint("views", __name__)
CORS(blueprint)


@blueprint.route("/employees/", methods=["POST"])
@validate()
def add_employee(body: NodeCreateModel):
    node = static_tree.add(body.name, body.parent_id)

    return {
        "data": node.to_dict(),
    }, 201


@blueprint.route("/employees/", defaults={"employee_id": None}, methods=["GET"])
@blueprint.route("/employees/<int:employee_id>/", methods=["GET"])
def employee(employee_id: int | None):
    node: Node | None = (
        static_tree.root if employee_id is None else static_tree.find(employee_id)
    )

    if not node:
        return "Employee not found", 404

    return {
        "data": node.to_dict(),
    }


@blueprint.route("/employees/<int:employee_id>/", methods=["PUT"])
@validate()
def update_employee(body: NodeCreateModel, employee_id: int):
    node = static_tree.find(employee_id)

    if not node:
        return "Employee not found", 404

    node.update(body.name)
    if node.parent and node.parent.id != body.parent_id:
        static_tree.set_parent(node, body.parent_id)

    return {
        "data": node.to_dict(),
    }
