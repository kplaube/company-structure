import logging

from .models import Node
from .utils import NodeCounter

logger = logging.getLogger(__name__)


class Tree:
    def __init__(self, root: Node):
        self.root = root

    def add(self, node_name: str, parent_id: int | None = None) -> Node:
        node = Node(node_name)

        if not parent_id:
            self.set_as_root(node)
            return self.root

        parent = self.get(parent_id)
        return parent.add_child(node)

    def find(self, node_id: int) -> Node | None:
        q = [self.root]

        while q:
            node = q.pop(0)

            if node.id == node_id:
                return node

            for child in node.children:
                q.append(child)

        return None

    def get(self, node_id: int) -> Node:
        node = self.find(node_id)
        if not node:
            raise Exception("Node not found")

        return node

    def reset(self):
        NodeCounter.reset()
        self.root = Tree.create().root

    def set_as_root(self, node: Node):
        self.set_parent(node, None)

    def set_parent(self, node: Node, parent_id: int | None):
        if node.parent:
            node.parent.remove_child(node)

        if parent_id:
            new_parent = self.get(parent_id)

            new_parent.add_child(node)
        else:
            logger.debug("A new root node has been defined: %s", node.name)

            tmp_node = self.root
            self.root = node
            self.root.parent = None
            self.root.add_child(tmp_node)

        return node

    @classmethod
    def create(cls) -> "Tree":
        root = Node("root")
        node_a = root.add_child(Node("a"))
        node_c = node_a.add_child(Node("c"))
        node_c.add_child(Node("d"))
        node_c.add_child(Node("e"))
        root.add_child(Node("b"))

        return cls(root)


static_tree = Tree.create()
