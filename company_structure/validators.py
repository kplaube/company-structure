from pydantic import BaseModel, validator

from .services import static_tree


class NodeCreateModel(BaseModel):
    name: str
    parent_id: int | None = None

    @validator("parent_id")
    def check_parent_id(cls, v):
        if v:
            if not static_tree.find(v):
                raise ValueError("Parent node not found")

        return v
